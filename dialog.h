#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
class Dialog;
}

typedef struct svrPara
{
    QString listenPort;
    QString toSvrIp;
    QString toSvrPort;
}svrParaSt;

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:
    void on_ok_clicked();

    void on_concel_clicked();

signals:
    void on_setPara(svrParaSt &);

private:
    Ui::Dialog *ui;

};

#endif // DIALOG_H
