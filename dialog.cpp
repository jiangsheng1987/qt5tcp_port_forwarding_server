#include "dialog.h"
#include "ui_dialog.h"
#include <QMessageBox>


Dialog::Dialog( QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    setWindowTitle("add forward svr");
    //ui->listenPort->setStyleSheet("border :1px ;background : (0x00,0xff,0x00,0x00)");
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_ok_clicked()
{
    if ( ui->listenPort->text().isEmpty() || \
         ui->forwardIp->text().isEmpty()  ||\
         ui->forwordPort->text().isEmpty() )
    {
        QMessageBox::warning(this, tr("forwardSvr warning"),
                                       tr("please check input\n"),
                                       QMessageBox::Ok );

        return;
    }

    svrParaSt svrPara;
    svrPara.listenPort = ui->listenPort->text();
    svrPara.toSvrIp    = ui->forwardIp->text();
    svrPara.toSvrPort  = ui->forwordPort->text();
    emit on_setPara(svrPara);

    this->close();
}

void Dialog::on_concel_clicked()
{
    this->close();
}
