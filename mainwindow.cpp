#include <QIcon>
#include <QFile>
#include <QMessageBox>
#include <QJsonDocument>
#include "mainwindow.h"
#include "ui_mainwindow.h"

//构造函数
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowTitle("TCP端口转发服务器");

    model = new QStandardItemModel(ui->forwardListView);
    model->setHorizontalHeaderLabels(QStringList()<<QStringLiteral("转发列表"));
    ui->forwardListView->setModel(model);
    ui->recvText->setReadOnly(true);
    ui->sendText->setReadOnly(true);
    initForwardSvrList();

    ui->hex->setChecked(true);
    ui->ascii->setChecked(false);
    ui->actionStart->setEnabled(true);
    ui->actionStop->setEnabled(false);
    ui->forwardListView->expandAll();

    m_setSvr = new Dialog;
    connect(m_setSvr, &Dialog::on_setPara, this, &MainWindow::recvAddSvrPara);
}

//析构函数
MainWindow::~MainWindow()
{
    delete ui;
}

//创建转发服务项参数结构
forwardParaSt * MainWindow::creatForwardSvr(int listenPort, QString toIp, int toPort)
{
    QString name;

    forwardParaSt *pforwardPara = new forwardParaSt;
    pforwardPara->listenPort = listenPort;
    pforwardPara->toIp = toIp;
    pforwardPara->toPort = toPort;
    name=QString("%1->%2:%3").arg(pforwardPara->listenPort).arg(pforwardPara->toIp).arg(pforwardPara->toPort);
    pforwardPara->itemProject = new QStandardItem(QIcon(QStringLiteral(":/icon/connection.png")),name);
    pforwardPara->itemProject->setEditable(false);
    return pforwardPara;
}

//从配置文件中，初始化转发服务列表
void MainWindow::initForwardSvrList()
{
    forwardParaSt *pforwardPara = NULL;
    CTaskService *pTmpTaskSer = NULL;

    QFile CfgFile(config_file);

    CfgFile.open(QIODevice::ReadOnly);
    QByteArray cfgData = CfgFile.readAll();

    QJsonParseError jsonError;//Qt5新类
    QJsonDocument json = QJsonDocument::fromJson(cfgData, &jsonError);//Qt5新类
    if ( jsonError.error != QJsonParseError::NoError )
    {
        ui->msg->setText("json file init fault!");
        return;
    }

    QJsonObject rootObj = json.object();
    if (rootObj.contains("svr"))
    {
        QJsonValue valueArray = rootObj.value("svr");
        QJsonArray jsonArray = valueArray.toArray();

        for (int i = 0; i < jsonArray.count();i++)
        {
            QJsonObject  childObject = jsonArray[i].toObject();
            int listenPort  = childObject.value("listenPort").toInt();
            QString toSvrIp = childObject.value("toSvrIp").toString();
            int toSvrPort   = childObject.value("toSvrPort").toInt();

            pforwardPara = creatForwardSvr(listenPort, toSvrIp, toSvrPort);
            m_forwardList[listenPort] = pforwardPara;

            pTmpTaskSer = new CTaskService(pforwardPara);
            m_TaskServiceList[pforwardPara->itemProject->text()] = pTmpTaskSer;
            connect(pTmpTaskSer, &CTaskService::sendLocalClientData, this, &MainWindow::localClientData);
            connect(pTmpTaskSer, &CTaskService::sendSvrClientData, this, &MainWindow::svrClientData);
            connect(pTmpTaskSer, &CTaskService::removeClient, this, &MainWindow::removeClientItem);
            connect(pTmpTaskSer, &CTaskService::addClient, this, &MainWindow::addClientItem);

            //pTmpTaskSer->startService();

            model->appendRow(m_forwardList[listenPort]->itemProject);

        }
    }

    return ;
}

//添加转发任务到配置文件中
void MainWindow::addForwardSvrToConfig(int listenPort, QString toSvrIp, int toSvrPort)
{
    QFile CfgFile(config_file);
    CfgFile.open(QIODevice::ReadOnly);
    QByteArray cfgData = CfgFile.readAll();
    CfgFile.close();

    QJsonParseError jsonError;//Qt5新类
    QJsonDocument json = QJsonDocument::fromJson(cfgData, &jsonError);//Qt5新类
    if ( jsonError.error != QJsonParseError::NoError )
    {
        ui->msg->setText("json file init fault!");
        //return;
    }

    QJsonObject jNode;
    jNode.insert("listenPort", QJsonValue(listenPort));
    jNode.insert("toSvrIp", QJsonValue(toSvrIp));
    jNode.insert("toSvrPort", QJsonValue(toSvrPort));


    QJsonObject rootObj = json.object();
    QJsonArray jsonArray;
    if (rootObj.contains("svr"))
    {
        jsonArray = rootObj.value("svr").toArray();
    }

    jsonArray.append(jNode);
    rootObj.insert("svr", jsonArray);

    qDebug()<<"jsonArray: "<<QJsonDocument(rootObj).toJson();

    CfgFile.open(QIODevice::WriteOnly);
    CfgFile.write(QJsonDocument(rootObj).toJson());
    CfgFile.close();
}

//删除指定端口的转发服务项，并回写配置
void MainWindow::delForwardSvrToConfig(int listenPort)
{
    QFile CfgFile(config_file);
    CfgFile.open(QIODevice::ReadOnly);
    QByteArray cfgData = CfgFile.readAll();
    CfgFile.close();

    QJsonParseError jsonError;//Qt5新类
    QJsonDocument json = QJsonDocument::fromJson(cfgData, &jsonError);//Qt5新类
    if ( jsonError.error != QJsonParseError::NoError )
    {
        ui->msg->setText("json file init fault!");
        return;
    }

    QJsonObject rootObj = json.object();
    if (rootObj.contains("svr"))
    {
        QJsonArray jsonArray = rootObj.value("svr").toArray();

        for (int i = 0; i < jsonArray.count();i++)
        {
            QJsonObject  childObject = jsonArray[i].toObject();
            if ( childObject.value("listenPort").toInt() == listenPort )
            {
                jsonArray.removeAt(i);
                break;
            }
        }

        rootObj.insert("svr", jsonArray);

        CfgFile.open(QIODevice::WriteOnly);
        CfgFile.write(QJsonDocument(rootObj).toJson());
        CfgFile.close();
    }
}


//获取父节点
QModelIndex MainWindow::getTopParent(const QModelIndex &itemIndex)
{
    QModelIndex secondItem = itemIndex;
    QModelIndex tmp = itemIndex;
    while(tmp.parent().isValid())
    {
        secondItem = tmp.parent();
        tmp = secondItem;
        qDebug()<<"now text: "<<secondItem.data().toString();
    }

    return secondItem;
}

void MainWindow::localClientData(QByteArray &data)
{
    if (ui->sendText->toPlainText().length() > 5242880)//内容大于5M时清零
    {
        ui->sendText->clear();
    }

    qDebug()<<"recv svr data size: "<<data.size();

    QDateTime dateTime;
    QString qstrTime= dateTime.currentDateTime().toString("yyyy-MM-dd hh:mm:ss");

    ui->sendText->append(qstrTime+":----------------recv-svr-data---------------size="+QString("%1").arg(data.size()));
    if ( bHexMod )
    {
        ui->sendText->append(data.toHex());
    }
    else
    {
        ui->sendText->append(data.data());
    }
}

//显示接收到的服务器端的数据
void MainWindow::svrClientData(QByteArray &data)
{
    if (ui->recvText->toPlainText().length() > 5242880)//内容大于5M时清零
    {
        ui->recvText->clear();
    }

    qDebug()<<"recv svr data size: "<<data.size();

    QDateTime dateTime;
    QString qstrTime= dateTime.currentDateTime().toString("yyyy-MM-dd hh:mm:ss");

    ui->recvText->append(qstrTime+":----------------recv-svr-data---------------size="+QString("%1").arg(data.size()));
    if ( bHexMod )
    {
        ui->recvText->append(data.toHex());
    }
    else
    {
        ui->recvText->append(data.data());
    }
}

//设置显示区域显示指定连接的交互数据
void MainWindow::selectPrintData(QString name, int id)
{
    if ( !nowPrintDataSerivice.isEmpty() )
    {
        QMap<QString,CTaskService*>::iterator iter = m_TaskServiceList.find(nowPrintDataSerivice);
        if ( iter != m_TaskServiceList.end() )
        {
            iter.value()->stopPrintData();
        }
    }

    m_TaskServiceList[name]->setSelectId(id);
    nowPrintDataSerivice = name;

    /*以下两行代码是用于在切换选择连接时，清空显示区域的数据显示*/
    ui->recvText->clear();
    ui->sendText->clear();
}

//单击转发列表事件处理
void MainWindow::on_forwardListView_clicked(const QModelIndex &index)
{
    QString str;
    str += QStringLiteral("当前选中：%1 row:%2,column:%3 ").arg(index.data().toString())
                          .arg(index.row()).arg(index.column());
    QString parentName = getTopParent(index).data().toString();
    qDebug()<<"parent: "<<parentName;
    ui->msg->setText(str);

    m_nowSelectSvrRow = index;

    if(parentName,index.data().toString() != parentName)
        selectPrintData(parentName,index.data().toString().toInt());
}

//选择ascii模式
void MainWindow::on_ascii_clicked()
{
    bHexMod = false;
}

//选择hex模式
void MainWindow::on_hex_clicked()
{
    bHexMod = true;
}

//清空服务端数据显示区
void MainWindow::on_recvDataClearButton_clicked()
{
    ui->recvText->clear();
}

//清空客户端数据显示区
void MainWindow::on_sendDataClearButton_clicked()
{
    ui->sendText->clear();
}

//启动服务按钮
void MainWindow::on_actionStart_triggered()
{
    ui->msg->setText("start service");
    ui->actionStart->setEnabled(false);
    ui->actionStop->setEnabled(true);

    QMap<QString,CTaskService*>::iterator iter;
    for ( iter = m_TaskServiceList.begin(); iter != m_TaskServiceList.end(); iter++)
    {
        iter.value()->startService();
        qDebug()<<"start "<<iter.key();
    }
}

//停止服务按钮处理
void MainWindow::on_actionStop_triggered()
{
    ui->msg->setText("stop service");
    ui->actionStart->setEnabled(true);
    ui->actionStop->setEnabled(false);
    QMap<QString,CTaskService*>::iterator iter;

    for ( iter = m_TaskServiceList.begin(); iter != m_TaskServiceList.end(); iter++)
    {
        iter.value()->stopService();
    }
}

//调出添加转发项对话框
void MainWindow::on_actionAdd_triggered()
{
    m_setSvr->show();
}

void MainWindow::recvAddSvrPara(svrParaSt &SvrPara)
{
    forwardParaSt *pforwardPara = NULL;
    CTaskService *pTmpTaskSer = NULL;


    QMap<int,forwardParaSt*>::iterator iter = m_forwardList.find(SvrPara.listenPort.toInt());
    if ( iter != m_forwardList.end() ) //already exist
    {
        QMessageBox::warning(this, tr("forwardSvr warning"),
                                       (SvrPara.listenPort+tr(" 已经存在\n")),
                                       QMessageBox::Ok );
        return;
    }

    pforwardPara = creatForwardSvr(SvrPara.listenPort.toInt(), SvrPara.toSvrIp, SvrPara.toSvrPort.toInt());
    m_forwardList[SvrPara.listenPort.toInt()] = pforwardPara;

    pTmpTaskSer = new CTaskService(pforwardPara);
    m_TaskServiceList[pforwardPara->itemProject->text()] = pTmpTaskSer;
    connect(pTmpTaskSer, &CTaskService::sendLocalClientData, this, &MainWindow::localClientData);
    connect(pTmpTaskSer, &CTaskService::sendSvrClientData, this, &MainWindow::svrClientData);
    connect(pTmpTaskSer, &CTaskService::removeClient, this, &MainWindow::removeClientItem);
    connect(pTmpTaskSer, &CTaskService::addClient, this, &MainWindow::addClientItem);

    if (!ui->actionStart->isEnabled())
       pTmpTaskSer->startService();

    model->appendRow(m_forwardList[SvrPara.listenPort.toInt()]->itemProject);
    addForwardSvrToConfig(SvrPara.listenPort.toInt(), SvrPara.toSvrIp, SvrPara.toSvrPort.toInt());
}

//删除转发服务项
void MainWindow::on_actionDel_triggered()
{
    QString forwardName = m_nowSelectSvrRow.data().toString();

    int dotPos = forwardName.indexOf("->");
    QString port = forwardName.left(dotPos);

    QMap<int,forwardParaSt*>::iterator iter = m_forwardList.find(port.toInt());
    if ( iter != m_forwardList.end() )
    {
        qDebug()<<"del "<<port<<" para";
        delete iter.value();
        m_forwardList.erase(iter);

    }

    QMap<QString,CTaskService*>::iterator iter1 = m_TaskServiceList.find(forwardName);
    if ( iter1 != m_TaskServiceList.end() )
    {
        qDebug()<<"del "<<forwardName;
        delete iter1.value();
        m_TaskServiceList.erase(iter1);
    }

    model->removeRow(m_nowSelectSvrRow.row());
    delForwardSvrToConfig(port.toInt());
}

//移除显示的连接列表
void MainWindow::removeItem(int id,QStandardItem *&svrItem)
{
    QStandardItem *ptmpItem = svrItem;

    ptmpItem = ptmpItem->child(0);

    while(ptmpItem)
    {
        if ( id == ptmpItem->text().toInt() )
        {
            qDebug()<<"find "<<id<<", row: "<<ptmpItem->index().row()<<" client, now remove";
            svrItem->removeRow( ptmpItem->index().row());
            break;
        }
        else
        {
            ptmpItem = svrItem->child(ptmpItem->row()+1);
            if(ptmpItem)
                qDebug()<<"get child id: "<<ptmpItem->text();

        }
    }

}

//移除指定客户端连接显示列表
void MainWindow::removeClientItem(int port, int id)
{
    QMap<int,forwardParaSt*>::iterator iter = m_forwardList.find(port);
    if ( iter != m_forwardList.end() )
    {
        removeItem(id, iter.value()->itemProject);
        qDebug()<<"del "<<port<<" id: "<<id;
    }
}

//添加新连接转发列表
void MainWindow::addClientItem(int port, int id)
{
    QMap<int,forwardParaSt*>::iterator iter = m_forwardList.find(port);
    if ( iter != m_forwardList.end() )
    {
        QString name = QString("%1").arg(id);
        QStandardItem* subItem = new QStandardItem(QIcon(QStringLiteral(":/icon/disconnect.png")),name);
        subItem->setEditable(false);
        iter.value()->itemProject->appendRow(subItem);
        qDebug()<<"add "<<port<<" id: "<<id;

        ui->forwardListView->expand(iter.value()->itemProject->index());
    }
}

//帮助。about
void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::about(this, "about", tr("TCP端口转发监控服务器    \n"
                                         "Email    :  wuquan-1230@163.com\n"
                                         "Author  :  wuwei\n"
                                         "Ver       :  V1.0.5\n"
                                         ));

}
