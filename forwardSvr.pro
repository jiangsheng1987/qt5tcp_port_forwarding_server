#-------------------------------------------------
#
# Project created by QtCreator 2018-04-09T15:54:16
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = forwardSvr
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    ctaskproc.cpp \
    ctaskservice.cpp \
    dialog.cpp

HEADERS  += mainwindow.h \
    ctaskproc.h \
    ctaskservice.h \
    dialog.h

FORMS    += mainwindow.ui \
    dialog.ui

RESOURCES += \
    resourcefile.qrc

RC_FILE+=icon/myapp.rc
